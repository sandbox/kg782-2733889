<?php

namespace Drupal\expose_entity_reference\Normalizer;

use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;
use Drupal\serialization\Normalizer\EntityReferenceFieldItemNormalizer;

class ExposeEntityReferenceFieldItemNormalizer extends EntityReferenceFieldItemNormalizer {

  /**
   * The interface or class that this Normalizer supports.
   *
   * @var string
   */
  protected $supportedInterfaceOrClass = EntityReferenceItem::class;

  /**
   * {@inheritdoc}
   */
  public function normalize($field_item, $format = NULL, array $context = []) {
    $values = parent::normalize($field_item, $format, $context);

    if ($entity = $field_item->get('entity')->getValue()) {
      if ($entity->getEntityTypeId() === 'node' ||
          $entity->getEntityTypeId() === 'media' ||
          $entity->getEntityTypeId() === 'file') {
        $values['target'] = $this->serializer->normalize($entity, $format, $context);
      }
    }

    return $values;
  }
}
